import firebase from 'firebase';
import 'firebaseui/dist/firebaseui.css';

const firebaseConfig = {
  apiKey: 'AIzaSyCzg8-xpUwPaEGQSsKRfeNS5JrV8T06ixU',
  authDomain: 'nabil-vue-tutorial.firebaseapp.com',
  projectId: 'nabil-vue-tutorial',
  storageBucket: 'nabil-vue-tutorial.appspot.com',
  messagingSenderId: '620574271419',
  appId: '1:620574271419:web:8279755d94eedd19f3c69e',
  measurementId:'G-H0LCGVBPTC',
};

firebase.initializeApp(firebaseConfig);
firebase.remoteConfig().settings.minimumFetchIntervalMillis = 5000;

export default firebase;
